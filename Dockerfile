FROM python:3.11.6

WORKDIR /app

# install dependencies
RUN pip install --upgrade pip 
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY consumer.py .

CMD [ "python", "consumer.py" ]