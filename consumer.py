from confluent_kafka import Consumer, KafkaException
from prometheus_client import start_http_server, Counter
import threading

conf = {
    'bootstrap.servers': 'localhost:9092',  
    'group.id': 'grp1',
    'auto.offset.reset': 'earliest'  
}

kafka_messages_received = Counter('kafka_messages_received', 'Number of messages received from Kafka')

def kafka_consumer():
    consumer = Consumer(conf)
    topics = ['kafka_shopee_topic'] 
    consumer.subscribe(topics)
    try:
        while True:
            # Poll for messages
            msg = consumer.poll(1.0)  # Specify the timeout (in seconds) for the poll method
         
            if msg is None:
                continue
            if msg.error():
                if msg.error().code() == KafkaException._PARTITION_EOF:
                    continue
                else:
                    print(msg.error())
                    break
             
            # Process the received message
            print('Received message: {}'.format(msg.value().decode('utf-8')))
            # Increment the Prometheus counter
            kafka_messages_received.inc()
    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()

if __name__ == '__main__':
    start_http_server(5050)
    # Start the Kafka consumer in a separate thread
    kafka_thread = threading.Thread(target=kafka_consumer)
    kafka_thread.start()

